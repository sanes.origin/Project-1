#!/bin/bash
count=0

echo "Content-type: text/html"
echo ""
echo "<html>"
curl http://localhost/menu/menu.txt
echo "<head><title>Мониторинг серверов</title></head><body>"
echo "<p>"
echo "Разработка группы Oracle/WAS"
echo "<br>ПТК. 2022г."
echo "</p>"

echo "<table border=0 align=left> "
echo "<TH colspan=10>Статус ссылок $(date +%F_%H:%M)</TH></TR>"

#############################################################

echo "<tr  valign=top><td><TABLE border=1 cellpadding=3>"
echo "<TH colspan=3>Ссылки комлекса</TH></TR>"
echo "<tr><th>Ссылки</th><th>RU</th><th>***</th></tr>"

cat test_hosts_list.txt | while read y

do

	echo "<tr><td>$y</td>"

    RU1=$(curl --write-out "%{http_code}\n" --silent --output /dev/null "https://$y.rzd.ru" --connect-timeout 3 --max-time 3)
	
        if [ $RU1 -ne 200 ];
                then
                        echo "<td bgcolor="red"><a href="https://$y.rzd.ru" target="_blank">$RU1</a></td>"
                else
                        echo "<td><a href="https://$y.rzd.ru" target="_blank">$RU1</a></td>"
         fi
		 
		 

    WEB1=$(curl --write-out "%{http_code}\n" --silent --output /dev/null "https://$y.web.rzd" --connect-timeout 3 --max-time 3)
	
        if [ $WEB1 -ne 200 ];
                then
                        echo "<td bgcolor="red"><a href="https://$y.web.rzd" target="_blank">$WEB1</a></td></tr>"
                else
                        echo "<td><a href="https://$y.web.rzd" target="_blank">$WEB1</a></td></tr>"
         fi
done
echo "</TABLE></td>"

#############################################################

echo "<td  valign=top><TABLE border=1 cellpadding=3>"
echo "<TH colspan=3>Сервера приложений</TH></TR>"
echo "<tr><td>Сервер</td><td>Кластер <br> TIMETABLE </td><td>Кластер <br>  TICKET</td></tr>"

for i in $(awk '$3~/tt-app/ && $1~/^1/ {print $3}' /etc/hosts); do 

	#echo $i ; 


echo "<tr><td>$i</td>"     

	# TicketCluster tt01-n01-srv02 
	#/opt/IBM/WebSphere/tt10-n01/logs/tt10-n01-srv02/tt10-n01-srv02.pid
	Ticket=$(curl --write-out "%{http_code}\n" --silent --output /dev/null "http://$i:19081/timetable/public/ru?STRUCTURE_ID=704&vp=8" --connect-timeout 3 --max-time 3)
	#http://10.248.10.161:19081/timetable/public/ru?STRUCTURE_ID=5251&vp=8
	#Ticket=$(curl --write-out "%{http_code}\n" --silent --output /dev/null "http://$i.gvc.oao.rzd:19081/timetable/public/ru?STRUCTURE_ID=5251&vp=8" --connect-timeout 3 --max-time 3)

        if [ $Ticket -ne 200 ];
                then
                        echo "<td bgcolor="red"><a href="http://$i.gvc.oao.rzd:19081/timetable/public/ru?STRUCTURE_ID=704\&vp=8" target="_blank">$Ticket</a></td>"
                else
                        echo "<td><a href="http://$i.gvc.oao.rzd:19081/timetable/public/ru?STRUCTURE_ID=704\&vp=8" target="_blank">$Ticket</a></td>"
         fi
	# Timetable Cluster tt01-n01-srv01 
	#/opt/ibm/websphere/tt10-n01/logs/tt10-n01-srv01/tt10-n01-srv01.pid
	Timetable=$(curl --write-out "%{http_code}\n" --silent --output /dev/null "http://$i.gvc.oao.rzd:19080/ticket/public/ru?vp=61" --connect-timeout 3 --max-time 3)
	#http://10.248.10.161:19080/ticket/public/ru?vp=61
        if [ $Timetable -ne 200 ];
                then
                        echo "<td bgcolor="red"><a href="http://$i.gvc.oao.rzd:19080/ticket/public/ru?vp=61" target="_blank">$Timetable </a></td>"
                else
                        echo "<td><a href="http://$i.gvc.oao.rzd:19080/ticket/public/ru?vp=61" target="_blank">$Timetable </a></td>"
         fi
#	arrTimetable[(( count++ ))]=$Timetable
echo "</tr>"
done
echo "</TABLE></td>"
############################################################# 
echo "<td  valign=top><TABLE border=1 cellpadding=3>"
echo "<TH colspan=4>Сервера приложений ИСВП</TH></TR>"
echo "<td>Сервер</td><td>Кластер <br> UNIVERSAL</td><td>Кластер <br> MEDIA  </td><td>Кластер <br> SEARCH   </td>"

for i in $(awk '$3~/us-app/ && $1~/^1/ {print $3}' /etc/hosts); do

echo "<tr><td>$i</td>"  
	# Uiniversal Cluster us07-n01-srv03 
	Uiniversal=$(curl --write-out "%{http_code}\n" --silent --output /dev/null "http://$i:19082/universal/public/ru?vp=9" --connect-timeout 3 --max-time 3)
        if [ $Uiniversal -ne 200 ];
                then
                        echo "<td bgcolor="red"><a href="http://$i.gvc.oao.rzd:19082/universal/public/ru?vp=9" target="_blank">$Uiniversal</a></td>"
                else
                        echo "<td><a href="http://$i.gvc.oao.rzd:19082/universal/public/ru?vp=9" target="_blank">$Uiniversal</a></td>"
         fi
	# Media Cluster  us07-n01-srv01 
	Media=$(curl --write-out "%{http_code}\n" --silent --output /dev/null "http://$i:19080/dbmm/images/9/121/4" --connect-timeout 3 --max-time 3)
        if [ $Media -ne 200 ];
                then
                        echo "<td bgcolor="red"><a href="http://$i.gvc.oao.rzd:19080/dbmm/images/9/121/4" target="_blank">$Media </a></td>"
                else
                        echo "<td><a href="http://$i.gvc.oao.rzd:19080/dbmm/images/9/121/4" target="_blank">$Media </a></td>"
         fi
	# Search Cluster  us07-n01-srv02 
	Search=$(curl --write-out "%{http_code}\n" --silent --output /dev/null "http://$i:19081/search/public/ru?vp=9" --connect-timeout 3 --max-time 3)
        if [ $Search -ne 200 ];
                then
                        echo "<td bgcolor="red"><a href="http://$i.gvc.oao.rzd:19081/search/public/ru?vp=9" target="_blank">$Search</a></td>"
                else
                        echo "<td><a href="http://$i.gvc.oao.rzd:19081/search/public/ru?vp=9" target="_blank">$Search</a></td>"
         fi
	#echo "$i Uiniversal=$Uiniversal Media=$Media Search=$Search "
	arrMedia[(( count++ ))]=$Media 
	arrUiniversal[(( count++ ))]=$Uiniversal
	arrSearch[(( count++ ))]=$Search
#echo "<tr><td>$i</td><td>$Uiniversal</td><td>$Media</td><td>$Search</td></tr>"
echo "</tr>"
done
echo "</TABLE></td>"
#############################################################
echo "<td  valign=top><TABLE border=1 cellpadding=3>"
echo "<TH colspan=4>Веб сервера Тикет</TH></TR>"
echo "<td>Сервер</td><td></td>"

for i in $(awk '$2~/tt-web/ && $1~/^1/ {print $2}' /etc/hosts); do

echo "<tr><td>$i</td>"  
        web=$(curl --write-out "%{http_code}\n" --silent --output /dev/null "http://$i/favicon.ico" --connect-timeout 3 --max-time 3)
        if [ $web -ne 200 ];
                then
                        echo "<td bgcolor="red"><a href="http://$i.gvc.oao.rzd/favicon.ico" target="_blank">$web</a></td>"
                else
                        echo "<td><a href="http://$i.gvc.oao.rzd/favicon.ico" target="_blank">$web</a></td>"
         fi
echo "</tr>"
done
echo "</TABLE></td>"
#############################################################
echo "<td  valign=top><TABLE border=0 cellpadding=3>"
echo "<tr><td><TABLE border=1 cellpadding=3>"
echo "<TH colspan=4>Веб сервера ИСВП</TH></TR>"
echo "<td>Сервер</td><td></td>"

for i in $(awk '$2~/us-web/ && $1~/^1/ {print $2}' /etc/hosts); do

echo "<tr><td>$i</td>"
        web=$(curl --write-out "%{http_code}\n" --silent --output /dev/null "http://$i/favicon.ico" --connect-timeout 3 --max-time 3)
        if [ $web -ne 200 ];
                then
                        echo "<td bgcolor="red"><a href="http://$i.gvc.oao.rzd/favicon.ico" target="_blank">$web</a></td>"
                else
                        echo "<td><a href="http://$i.gvc.oao.rzd/favicon.ico" target="_blank">$web</a></td>"
         fi
echo "</tr>"
done
echo "</TABLE></td></tr>"



#############################################################
echo "<tr  valign=top><td><TABLE border=1 cellpadding=3>"
echo "<TH colspan=2>Сервера приложений <br>"Админ зоны"</TH></TR>"
echo "<td>Сервер</td><td></td>"

for i in $(awk '$2~/ps-app/ && $1~/^1/ {print $2}' /etc/hosts); do

echo "<tr><td>$i</td>"
        web=$(curl --write-out "%{http_code}\n" --silent --output /dev/null "http://$i:19082/wps/portal/" --connect-timeout 3 --max-time 3)
        if [ $web -ne 200 ];
                then
                        echo "<td bgcolor="red"><a href="http://$i.gvc.oao.rzd:19082/wps/portal/" target="_blank">$web</a></td>"
                else
                        echo "<td><a href="http://$i.gvc.oao.rzd:19082/wps/portal/" target="_blank">$web</a></td>"
         fi
echo "</tr>"
done
echo "</TABLE></td></tr>"

#############################################################
echo "<tr><td  valign=top><TABLE border=1 cellpadding=3>"
echo "<TH colspan=2>Веб сервера <br>"Админ зоны"</TH></TR>"
echo "<td>Сервер</td><td></td>"

for i in $(awk '$2~/ps-web/ && $1~/^1/ {print $2}' /etc/hosts); do

echo "<tr><td>$i</td>"
        web=$(curl --write-out "%{http_code}\n" --silent --output /dev/null "http://$i/favicon.ico" --connect-timeout 3 --max-time 3)
        if [ $web -ne 200 ];
                then
                        echo "<td bgcolor="red"><a href="http://$i.gvc.oao.rzd/favicon.ico" target="_blank">$web</a></td>"
                else
                        echo "<td><a href="http://$i.gvc.oao.rzd/favicon.ico" target="_blank">$web</a></td>"
         fi
echo "</tr>"
done

echo "</TABLE></td></tr>"  
echo "</TABLE></td>"
     

#echo "Ticket ${arrTicket[@]}  Timetable= ${arrTimetable[@]}"
 echo "</body></html>"
