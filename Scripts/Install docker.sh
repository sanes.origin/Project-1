#!/usr/bin/env bash

ENV="";
if [ "${3}" == 'preprod' -o "${3}" == 'prod' ]; then
    ENV="${3}";
fi;

if [ -z "${1}" -o -z "${2}" -o -z "${ENV}" ]; then
    echo -e "
    \033[1;31mERROR: Invalid params\033[21;0m

    Usage:
        install-prod.sh tcp 192.168.168.168:2375 preprod
        install-prod.sh unix /var/run/docker.sock preprod
        install-prod.sh tcp 192.168.168.168:2375 prod
        install-prod.sh unix /var/run/docker.sock prod
    ";
    exit 1;
fi;

if [ "${1}" == 'tcp' ]; then
    PARAMS=" -e \"DOCKER_HOST=tcp://${2}\" ";
else
    PARAMS=" -v ${2}:/var/run/docker.sock ";
fi;

cd "$( cd "$( dirname "$0" )" && pwd )" || exit;


APPPWD="$( cd "$( dirname "$0" )" && pwd )";


echo 'Importing composer image...';
docker image load -i ./composer.tar;
docker run --rm ${PARAMS} -e "HOSTNAME=$(hostname)" -v $(pwd):/app \
    --env-file ./.project.env --env-file "./.project.env-${ENV}" \
    -e "APPPWD=${APPPWD}"  -e "LOGSTASH_IMAGE_PP=rzd/dummy" -e"LOGSTASH_IMAGE=rzd/dummy" \
    -it tmaier/docker-compose sh -c \
    "cd /app; \
        echo 'Removing old service...'; \
        docker-compose down --rmi all 2>/dev/null; \
        echo 'Importing project images...' && \
        docker image load -i ./rzd.tar && \
        docker-compose run --rm ss-${ENV} --data-dirs --migrations --cache --init-config --change-config --set-version echo Done install && \
        docker-compose up -d ss-${ENV}" \
;
