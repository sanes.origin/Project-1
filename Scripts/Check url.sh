#!/bin/bash
count=0

echo "Content-type: text/html"
echo ""
echo "<html>"
curl http://localhost/menu/menu.txt
echo "<head><title>Мониторинг серверов</title></head><body>"
echo "<p>"
echo "Желонкин А.А. (Группа Oracle, WAS)"
echo "<br>ПТК. 2022г."
echo "</p>"

echo "<table border=0 align=left> "
echo "<TH colspan=10>Статус ссылок *.RZD.RU и *.WEB.RZD на $(date +%F_%H:%M)</TH></TR>"

#############################################################

echo "<tr  valign=top><td><TABLE border=1 cellpadding=3>"
echo "<TH colspan=3>Ссылки комлекса</TH></TR>"
echo "<tr><th>Ссылки</th><th>Внешняя RU</th><th>СПД RZD</th></tr>"

buffer=1

cat hosts_list.txt | while read y

do

ost=$(( $buffer % 2 ))

if [ $ost -ne 0 ]
then

        echo "<tr><td>$y</td>"

    RU1=$(curl --write-out "%{http_code}\n" --silent --output /dev/null "$y" --connect-timeout 3 --max-time 3)

        if [ $RU1 -ne 200 ];
                then
                        echo "<td bgcolor="red"><a href="$y" target="_blank">$RU1</a></td>"
                else
                        echo "<td><a href="$y" target="_blank">$RU1</a></td>"
         fi
                 
else
                 

    WEB1=$(curl --write-out "%{http_code}\n" --silent --output /dev/null "$y" --connect-timeout 3 --max-time 3)

        if [ $WEB1 -ne 200 ];
                then
                        echo "<td bgcolor="red"><a href="$y" target="_blank">$WEB1</a></td></tr>"
                else
                        echo "<td><a href="$y" target="_blank">$WEB1</a></td></tr>"
         fi
fi
buffer=$(( $buffer+1 ))
done
echo "</TABLE></td>"

#############################################################




 echo "</body></html>"

